# Readme
## 1- Introduction
This package is designed for EAGLE M.Sc. students. The package(steigerwald), is so far a data package (no scripts included yet). It contains:

  * a list (bio_data) with three data-frames:                                                 
  [1] forest_short: Small subset of forest data with information from LIDAR data included
  [2] forest_long: The whole forest data without LIDAR data
  [3] bats: Bats survey data table

  * 3 separate shape files                                                                  
  [] buildings      (Multipolygon)                                                          
  [] roads          (Multiline)                                                             
  [] survey_points  (Multipoints)
  
  * A raster stack                                                                          
  [] timescan     


## 2- Getting Started

The following instructions will get you a copy of the package data on your local machine. 

### 2.1 Prerequisites

In order to install the package, first need to have the package 'devtools' installed and loaded:
```
install.packages("devtools")
library(devtools)
```

### 2.2 Installing

Load the package with data from Bitbucket using the following command:
```
install_bitbucket("EAGLE_MSc/steigerwald", build_vignettes=TRUE)
```
Then load the package and the data with:
```
library(steigerwald)
data("bio_data")
bio_data
```
For accessing help, type:
```
steigerwald
```
### 3- Content

The package contains a list of three data-frames [1]forest_short, [2]forest_long, and [3]bats. Abbreviation of columns names is as the following:


* bio_data$forest_short
```
lon_x          Longtitude (WGS84)
lat_y          Latitude (WGS84)
beech          Cross section area of Beech trees basal area per plot in m2
oak            Cross section area of Oak trees basal area per plot in m2
ndvi           Pixel ndvi value
ts_ndbi_max    TimeScan NDBI maximum
ts_ndbi_min    TimeScan NDBI minimum
ts_ndbi_avg    TimeScan NDBI average
ts_ndbi_stdev  TimeScan NDBI standard deviation
ts_ndbi_mean   TimeScan NDBI mean slope (temporal variability of spectral indices)
ts_mndvi_max   TimeScan mNDVI maximum
ts_mndvi_min   TimeScan mNDVI minimum
ts_mndvi_avg   TimeScan mNDVI average
ts_mndvi_stdev TimeScan mNDVI standard deviation
ts_mndvi_mean  TimeScan mNDVI mean slope
ts_mndvi_max   TimeScan mNDVI maximum
ts_ndvi_min    TimeScan NDVI minimum
ts_ndvi_avg    TimeScan NDVI average
ts_ndvi_stdev  TimeScan NDVI standard deviation
ts_ndvi_mean   TimeScan NDVI mean slop
ts_ndvi_max    TimeScan ND57 maximum
ts_nd57_min    TimeScan ND57 minimum
ts_nd57_avg    TimeScan ND57 average
ts_nd57_stdev  TimeScan ND57 standard deviation
ts_nd57_mean   TimeScan ND57 mean slope
ts_nd57_max    TimeScan ND42 maximum
ts_nd42_min    TimeScan ND42 minimum
ts_nd42_avg    TimeScan ND42 average
ts_nd42_stdev  TimeScan ND42 standard deviation
ts_nd42_mean   TimeScan ND42 mean slope
ts_nd42_max    TimeScan ND32 maximum
ts_nd32_min    TimeScan ND32 minimum
ts_nd32_avg    TimeScan ND32 average
ts_nd32_stdev  TimeScan ND32 standard deviation
ts_nd32_mean   TimeScan ND32 mean slope
lidar_averg    LIDAR average intensity value
lidar_max      LIDAR maximum intensity value
lidar_min      LIDAR minumum intensity value
sub_basin      Watershed basin
height         Height at each point(representing forest height per sq.meter)
```


* bio_data$forest_long
```
lon_x          Longtitude (WGS84)
lat_y          Latitude (WGS84)
sycamore       Cross section area of Sycamore trees basal area per plot in m2
beech          Cross section area of Beech trees basal area per plot in m2
dougals        Cross section area of Dougals Fir trees basal area per plot in m2
oak            Cross section area of Oak trees basal area per plot in m2
ash            Cross section area of Ash trees basal area per plot in m2
spruce         Cross section area of Spruce trees basal area per plot in m2
hornbeam       Cross section area of Hornbeam trees basal area per plot in m2
pine           Cross section area of Pine trees basal area per plot in m2
larch          Cross section area of Larch trees basal area per plot in m2
lime           Cross section area of Lime trees basal area per plot in m2
black          Cross section area of Black Alder trees basal area per plot in m2
deadwood       Cross section area of Deadwood trees basal area per plot in m2
barkdamage     Cross section area of Barkdamage trees basal area per plot in m2
ts_ndbi_max    TimeScan NDBI maximum
ts_ndbi_min    TimeScan NDBI minimum
ts_ndbi_avg    TimeScan NDBI average
ts_ndbi_stdev  TimeScan NDBI standard deviation
ts_ndbi_mean   TimeScan NDBI mean slope (temporal variability of spectral indices)
ts_mndvi_max   TimeScan mNDVI maximum
ts_mndvi_min   TimeScan mNDVI minimum
ts_mndvi_avg   TimeScan mNDVI average
ts_mndvi_stdev TimeScan mNDVI standard deviation
ts_mndvi_mean  TimeScan mNDVI mean slope
ts_mndvi_max   TimeScan mNDVI maximum
ts_ndvi_min    TimeScan NDVI minimum
ts_ndvi_avg    TimeScan NDVI average
ts_ndvi_stdev  TimeScan NDVI standard deviation
ts_ndvi_mean   TimeScan NDVI mean slop
ts_ndvi_max    TimeScan ND57 maximum
ts_nd57_min    TimeScan ND57 minimum
ts_nd57_avg    TimeScan ND57 average
ts_nd57_stdev  TimeScan ND57 standard deviation
ts_nd57_mean   TimeScan ND57 mean slope
ts_nd57_max    TimeScan ND42 maximum
ts_nd42_min    TimeScan ND42 minimum
ts_nd42_avg    TimeScan ND42 average
ts_nd42_stdev  TimeScan ND42 standard deviation
ts_nd42_mean   TimeScan ND42 mean slope
ts_nd42_max    TimeScan ND32 maximum
ts_nd32_min    TimeScan ND32 minimum
ts_nd32_avg    TimeScan ND32 average
ts_nd32_stdev  TimeScan ND32 standard deviation
ts_nd32_mean   TimeScan ND32 mean slope
basin_no       Watershed basin
```

* bio_data$bats
```
lon_x           Longtitude (WGS84)
lat_y           Latitude (WGS85)
survey_point    Names of survey points
sp_closed       Total number of closed foraging bats species 
sp_edge         Total number of edge foraging bats species
sp_open         Total number of open foraging bats species
sp_closed_perc  Precentage of closed foraging bats species in each survey point
sp_edge_perc    Precentage of edge foraging bats species in each survey point
sp_open_perc    Precentage of open foraging bats species in each survey point
sp_total        The total number of recorded species at each survey point
mean_ndvi       Mean NDVI value of cells overlaping with 30m buffer radius around each point 
farm_prec       Farmland percentage of area covring 30m buffer radius from each point points
forest_perc     Forest percentage of area covring 30m buffer radius from each point points
meadows_prec    Meadows percentage of area covring 30m buffer radius from each point points
lc_type         Landcover type of the point location (no buffer, just the point)
ndvi            NDVI value of the point location (the pixel NDVI behind the point)
ts_ndbi_max     TimeScan NDBI maximum
ts_ndbi_min     TimeScan NDBI minimum
ts_ndbi_avg     TimeScan NDBI average
ts_ndbi_stdev   TimeScan NDBI standard deviation
ts_ndbi_mean    TimeScan NDBI mean slope (temporal variability of spectral indices)
ts_mndvi_max    TimeScan mNDVI maximum
ts_mndvi_min    TimeScan mNDVI minimum
ts_mndvi_avg    TimeScan mNDVI average
ts_mndvi_stdev  TimeScan mNDVI standard deviation
ts_mndvi_mean   TimeScan mNDVI mean slope
ts_ndvi_max     TimeScan NDVI maximum
ts_ndvi_min     TimeScan NDVI minimum
ts_ndvi_avg     TimeScan NDVI average
ts_ndvi_stdev   TimeScan NDVI standard deviation
ts_ndvi_mean    TimeScan NDVI mean slop
ts_nd57_max     TimeScan ND57 maximum
ts_nd57_min     TimeScan ND57 minimum
ts_nd57_avg     TimeScan ND57 average
ts_nd57_stdev   TimeScan ND57 standard deviation
ts_nd57_mean    TimeScan ND57 mean slope
ts_nd42_max     TimeScan ND42 maximum
ts_nd42_min     TimeScan ND42 minimum
ts_nd42_avg     TimeScan ND42 average
ts_nd42_stdev   TimeScan ND42 standard deviation
ts_nd42_mean    TimeScan ND42 mean slope
ts_nd32_max     TimeScan ND32 maximum
ts_nd32_min     TimeScan ND32 minimum
ts_nd32_avg     TimeScan ND32 average
ts_nd32_stdev   TimeScan ND32 standard deviation
ts_nd32_mean    TimeScan ND32 mean slope
```
